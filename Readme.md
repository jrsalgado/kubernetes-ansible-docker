# Provision and Run K8's Master and Nodes with Ansible from a docker container

1. First build the image where is installed ansible
```bash
docker build -t my-ansible .
```

2. Create a key to ssh into the servers you will be execute ansible commands into
```bash
ssh-keygen  # store the key on project directory $PWD/.ssh/id_rsa
``` 

3. Run the my-ansible container with the directory ./myplatform as volume
```bash
# the entrypoint is 'ansible-playbook'
docker run -it --rm -v "$PWD"/myplatform:/myplatform playbook-master.yml --list-host
```
