FROM ubuntu:16.04

RUN apt-get update -y \
    && apt-get install -y software-properties-common vim\
    && apt-add-repository ppa:ansible/ansible \
    && apt-get update -y \
    && apt-get install -y ansible \
    && cp -R /etc/ansible myplatform

COPY .ssh /root/.ssh

WORKDIR myplatform

ENTRYPOINT [ "ansible-playbook" ]